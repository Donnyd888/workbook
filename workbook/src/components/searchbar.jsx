import React from 'react';

const SearchBar = () => {
  return (
    <div className="search-bar-container">
      <i className="material-icons">search</i>
      <input type="text" className="search-bar" placeholder="Search Google" />
    </div>
  );
};

export default SearchBar;
