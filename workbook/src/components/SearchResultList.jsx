import SearchResultItem from './SearchResultItem';

const SearchResultList = ({ results }) => {
  return (
    <div className="search-result-list">
      {results.map(result => (
        <SearchResultItem
          key={result.id}
          title={result.title}
          snippet={result.snippet}
          url={result.url}
        />
      ))}
    </div>
  );
};

export default SearchResultList;