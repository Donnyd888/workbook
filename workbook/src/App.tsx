import './App.css';

const SearchBar = () => {
  return (
    <div className="search-bar-container">
      <i className="material-icons">search</i>
      <input type="text" className="search-bar" placeholder="Search Google" />
    </div>
  );
};

const SearchResultItem = ({ title, snippet, url }) => {
  return (
    <div className="search-result-item">
      <h3>{title}</h3>
      <p>{snippet}</p>
      <a href={url}>{url}</a>
    </div>
  );
};

const SearchResultList = ({ results }) => {
  return (
    <div className="search-result-list">
      {results.map(result => (
        <SearchResultItem
          key={result.id}
          title={result.title}
          snippet={result.snippet}
          url={result.url}
        />
      ))}
    </div>
  );
};

const searchResults = [
  {
    id: 1,
    title: "React",
    snippet: "React is the library for web and native user interfaces. Build user interfaces out of individual pieces called components written in JavaScript..",
    url: "https://react.dev"
  },
  {
    id: 2,
    title: "React – A JavaScript library for building user interfaces",
    snippet: "React makes it painless to create interactive UIs. Design simple views for each state in your application, and React will efficiently update and render just ...",
    url: "https://legacy.reactjs.org"
  },
  {
    id: 3,
    title: "React (software)",
    snippet: "React is a free and open-source front-end JavaScript library for building user interfaces based on components. It is maintained by Meta (formerly Facebook) ...",
    url: "https://en.wikipedia.org › wiki › React_(software)"
  },

  {
    id: 4,
    title: "Getting Started",
    snippet: "This page is an overview of the React documentation and related resources. React is a JavaScript library for building user interfaces. Learn what React is ...",
    url: "https://en.wikipedia.org › wiki › React_(software)"
  },

  {
    id: 5,
    title: "What is React",
    snippet: "It is a a development server that uses Webpack to compile React, JSX, and ES6, auto-prefix CSS files. The Create React App uses ESLint ...",
    url: "https://www.w3schools.com › whatis › whatis_react)"
  },
  {
    id: 6,
    title: "React Definition & Meaning",
    snippet: "The meaning of REACT is to exert a reciprocal or counteracting force or influence —often used with on or upon. How to use react in a sentence.",
    url: "https://www.merriam-webster.com › dictionary › react"
  },

  {
    id: 7,
    title: "What Is React? [Easily Explained]",
    snippet: "Oct 5, 2023 — React is a JavaScript-based UI development library. Although React is a library rather than a language, it is widely used in web development.",
    url: "ttps://www.simplilearn.com › tutorials › reactjs-tutorial"
  },
  
]; 

function App() {


  return (
    <div className="app-container">
      <header>
        <h1>Google</h1>
      </header>
      <main>
        <div className="search-container">
          <SearchBar />
         
        </div>
        <SearchResultList results={searchResults} />
      </main>
    </div>
  );
}

export default App;
